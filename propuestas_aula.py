import math

def promedio():
    num1 = int(input("Ingrese el primer número: "))
    num2 = int(input("Ingrese el segundo numero número: "))
    print("El promedio de {0} y {1} es: {2}".format(num1, num2, (num1+num2)/2))

def perimetro_triangulo():
    print("falta ejercicio")

def area_triangulo():
    base = int(input("Ingrese la base del triangulo: "))
    altura = int(input("Ingrese la altura del triangulo: "))
    print("El area del triangulo con base {0} y altura {1} es: {2}".format(base, altura, (base*altura)/2))

def saldo_caja():
    saldo = int(input("Ingrese el saldo de la caja de ahorros: "))
    monto = int(input("Ingrese el monto a retirar: "))
    print("El saldo de la caja de ahorros era {0} y se retiraron {1}. El nuevo saldo es: {2}".format(saldo, monto, saldo-monto))

def pulgadas_centimetros():
    pulgadas = int(input("Ingrese el valor en pulgadas: "))
    print("{0} pulgada/s son {1} centimetro/s".format(pulgadas, pulgadas*2.54))

def main():
    #Ingresar dos numeros, calcular y mostrar el promedio
    print("Ingresar dos numeros, calcular y mostrar el promedio")
    promedio()
    #Ingresar la base y la altura de un triangulo, calcular y mostrar el perímetro
    print("Ingresar la base y la altura de un triangulo, calcular y mostrar el perímetro")
    perimetro_triangulo()
    #Ingresar la base y la altura de un triangulo, calcular y mostrar el área
    print("Ingresar la base y la altura de un triangulo, calcular y mostrar el área")
    area_triangulo()
    #Ingresar el saldo de una caja de ahorros, el importe a retirar y mostrar el nuevo saldo en cuenta
    print("Ingresar el saldo de una caja de ahorros, el importe a retirar y mostrar el nuevo saldo en cuenta")
    saldo_caja()
    #Ingresar un número en pulgadas y mostrarlo en centimetros
    print("Ingresar un número en pulgadas y mostrarlo en centimetros")
    pulgadas_centimetros()


if __name__ == '__main__':
    main()